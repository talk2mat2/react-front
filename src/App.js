
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import './App.css';
import Detailview from './components/pages/detailview';
import HistoricGraph from './components/pages/historicGraph';
import Home from './components/pages/home';
import ViewHistory from './components/pages/ViewHistory';

function App() {
  return (
    <Router>
      <Switch>
      <Route exact path="/">
      <Home/>
    </Route>
      <Route exact path="/Detailview/:places_id" component={ Detailview}/>
      <Route path="/ViewHistory">
        <ViewHistory />
      </Route>
      <Route exact path="/HistoricGraph/:places_id" component={ HistoricGraph}/>
      </Switch>
  
  </Router>
  );
}

export default App;
