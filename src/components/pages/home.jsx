import React from "react";
import Navbar from "../navbar";
import { GoogleAutoComplete } from "../../api";
import { useHistory } from "react-router";
import Footer from "../footer";

const Home = () => {
  const [searchInput, setSearchInput] = React.useState("");
  const [searchResult, setSearchResult] = React.useState([]);
  const history = useHistory();

  const handleSearch = async (e) => {
    setSearchInput(e.target.value);
    const query = e.target.value;
    query &&
      (await GoogleAutoComplete(query)
        .then((response) => setSearchResult(response.data.predictions))
        .catch((error) => console.log("error occured", error)));
    !query && setSearchResult([]);
  };
  const handleSelected = (places) => {
    const { place_id, description } = places;
    // console.log(places);
    setSearchResult([]);
    history.push(`/Detailview/${place_id}?description=${description}`);
  };
  const mapSearchResult = () => {
    //description
    return (
      searchResult.length > 0 &&
      searchResult.map((places, index) => (
        <div
          onClick={handleSelected.bind(this, places)}
          key={index}
          className="resultCard"
        >
          {places.description}
        </div>
      ))
    );
  };

  return (
    <div>
      <Navbar />

      <div className=" home-slider mt-2 ">
        <div className="col-12 slider-row ">
          <div className="overLaySlider">
            <div className="col-lg-12  d-flex flex-column align-items-center ">
              <h2 className="slider-title">Let's Search The Weather Now</h2>
              <div className="searchContainer">
                {searchResult.length > 0 ? (
                  <div className="resultOverlay">{mapSearchResult()}</div>
                ) : null}
                <input
                  value={searchInput}
                  onChange={handleSearch}
                  className="slider-input"
                  type="text"
                  placeholder="weather Location"
                />
              </div>
            </div>
          </div>

          <img src="/slider/2.jpg" className="weathimg" alt="" />
        </div>
      </div>
      <div id="about-us" className="container my-80 about-us">
        <div className="row">
          <div className="col-lg-12 col-sm-8">
            <div className="mb-3">
              <h4 className="sub-title">About weather man</h4>
              <div className="line1" />
            </div>

            <p className="sub-para mt-5">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
              faucibus metus et eros bibendum, ac egestas tortor tempus. Mauris
              viverra dolor eu dignissim elementum. Cras quis mollis lacus. Sed
              aliquam blandit augue, vitae cursus urna vestibulum et. Praesent
              faucibus eros eget malesuada viverra. Proin fermentum, sapien et
              sodales tempus, lorem tortor mattis risus, eu tincidunt nunc urna
              vitae risus. Fusce sed dui nunc. Sed at felis
            </p>
          </div>
        </div>
      </div>
      <div className=" my-80 why-us back-primary">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-sm-8  ">
              <div className="mb-3 d-flex flex-column align-items-center  text-center ">
                <h4 className="sub-title ">Why Weather man</h4>
                <div className="line1 " />
              </div>

              <p className="sub-para text-center mt-5">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
                faucibus metus et eros bibendum, ac egestas tortor tempus.
                Mauris viverra dolor eu dignissim elementum. Cras quis mollis
                lacus. Sed aliquam blandit augue, vitae cursus urna vestibulum
                et. Praesent faucibus eros eget malesuada viverra. Proin
                fermentum, sapien et sodales tempus, lorem tortor mattis risus,
                eu tincidunt nunc urna vitae risus. Fusce sed dui nunc. Sed at
                felis
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="container my-80 about-us">
        <div className="row">
          <div id="howitworks" className="col-lg-12 col-sm-8">
            <div className="mb-3">
              <h4 className="sub-title">How weather man works</h4>
              <div className="line1" />
            </div>

            <p className="sub-para mt-5">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
              faucibus metus et eros bibendum, ac egestas tortor tempus. Mauris
              viverra dolor eu dignissim elementum. Cras quis mollis lacus. Sed
              aliquam blandit augue, vitae cursus urna vestibulum et. Praesent
              faucibus eros eget malesuada viverra. Proin fermentum, sapien et
              sodales tempus, lorem tortor mattis risus, eu tincidunt nunc urna
              vitae risus. Fusce sed dui nunc. Sed at felis
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
