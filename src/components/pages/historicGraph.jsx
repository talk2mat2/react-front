import React from "react";
import { useParams, useHistory } from "react-router";
import queryString from "query-string";

import Navbar from "../navbar";
import { Historicweatherinfo } from "../../api";
import CanvasJSReact from "../../canvasjs.react";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const HistoricGraph = ({ location: { search } }) => {
  const history = useHistory();
  const { places_id } = useParams();
  const [dataResult, setDataResult] = React.useState([]);
  const description = queryString.parse(search).description || "";

  React.useEffect(async () => {
    places_id &&
      (await Historicweatherinfo(places_id)
        .then((response) => {
          // console.log(response.data);
          setDataResult(response.data);
        })
        .then(() => {})
        .catch((error) => console.log(error)));
  }, []);

  const Temperature = () => {
    return (
      dataResult.length > 0 &&
      dataResult.map((item) => {
        const time = new Date(item.dt * 1000);
        const date = time.getDate();
        const month = time.getMonth();
        const year = time.getFullYear();
        return { y: item["temp"], label: date + "/" + month + "/" + year };
      })
    );
  };
  const DewPoint = () => {
    return (
      dataResult.length > 0 &&
      dataResult.map((item) => {
        const time = new Date(item.dt * 1000);
        const date = time.getDate();
        const month = time.getMonth();
        const year = time.getFullYear();
        return { y: item["dew_point"], label: date + "/" + month + "/" + year };
      })
    );
  };
  const humidity = () => {
    return (
      dataResult.length > 0 &&
      dataResult.map((item) => {
        const time = new Date(item.dt * 1000);
        const date = time.getDate();
        const month = time.getMonth();
        const year = time.getFullYear();
        return { y: item["humidity"], label: date + "/" + month + "/" + year };
      })
    );
  };

  const options = {
    animationEnabled: true,
    title: {
      text: "Temperature, dew point and Humidity",
    },
    axisY: {
      title: "oF",
    },
    toolTip: {
      shared: true,
    },
    data: [
      {
        type: "spline",
        name: "Temperature",
        showInLegend: true,
        dataPoints: Temperature(),
      },
      {
        type: "spline",
        name: "Dew point",
        showInLegend: true,
        dataPoints: DewPoint(),
      },
      {
        type: "spline",
        name: "humidity",
        showInLegend: true,
        dataPoints: humidity(),
      },
    ],
  };
  return (
    <div>
      <Navbar />
      {/* <p>{places_id}</p> */}
      <div className="detailpage my-80">
        <div className="container">
          <div className=" back-primary detail-main">
            <div className="col-lg-10 mb-5">
              {" "}
              <h3>5 Day Historic Weather Data for {description}</h3>
              <div className="line3" />
              <div style={{ float: "right" }} className="col-4 ">
                <button
                  onClick={() => history.goBack()}
                  className="btn btn-secondary"
                >
                  {" "}
                  back
                </button>
              </div>
            </div>
            <div className="col-lg-12 pb-3">
              <CanvasJSChart options={options} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HistoricGraph;
