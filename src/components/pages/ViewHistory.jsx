import React from "react";
import { Link } from "react-router-dom";

import Navbar from "../navbar";
const ViewHistory = () => {
  // const [dataResult, setDataResult] = React.useState({});
  // const { places_id } = useParams();
  // const description = queryString.parse(search).description || "";
  const localSavedData = localStorage.getItem("ViewHistory");
  const ParsedData = localSavedData ? JSON.parse(localSavedData) : [];
  const [historyData] = React.useState([...ParsedData]);

  const mapHistoryData = () =>
    historyData.length > 0
      ? historyData.map((item, index) => (
          <li key={index}>
            <Link
              to={`/Detailview/${item.places_id}?description=${item.description}`}
            >
              {item.description}
            </Link>
          </li>
        ))
      : null;

  return (
    <div>
      <Navbar />
      {/* <p>{places_id}</p> */}
      <div className="detailpage my-80">
        <div className="container">
          <div className=" back-primary detail-main">
            <div className="col-lg-8 mb-5">
              <h3>Weather Search History</h3>
              <div className="line3" />
            </div>

            {historyData.length > 0 ? (
              <div className="">
                <h6>
                  Last 5 weather Search <b>locations</b>
                </h6>
                <ol className="history-list">{mapHistoryData()}</ol>
              </div>
            ) : (
              <div className="col-lg-12 mb-1 d-flex flex-column align-items-center">
                <h4 className="mb-2">You have not made any search : ) </h4>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewHistory;
