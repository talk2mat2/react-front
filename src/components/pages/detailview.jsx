import React from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import queryString from "query-string";
import { Getweatherinfo } from "../../api";
import Navbar from "../navbar";
const Detailview = ({ location: { search } }) => {
  const [dataResult, setDataResult] = React.useState({});
  const { places_id } = useParams();
  const description = queryString.parse(search).description || "";

  //save search history to localstorage
  const CreateViewHistory = (places_id, description) => {
    const localData = localStorage.getItem("ViewHistory");
    const newHistory = { places_id, description };
    //if it exist we append t existing
    if (localData) {
      let parsedData = JSON.parse(localData);
      // dont save more than 5 items to search history
      if (parsedData.length < 5) {
        parsedData.unshift(newHistory);
        localStorage.setItem("ViewHistory", JSON.stringify(parsedData));
      } else {
        //if its up to 5 we remove the last one
        parsedData.pop();
        parsedData.unshift(newHistory); //then add to the begining
        localStorage.setItem("ViewHistory", JSON.stringify(parsedData));
      }
    } else {
      //if it doesnt ,we create new one and save
      let newLocalData = [];
      newLocalData.push(newHistory);
      localStorage.setItem("ViewHistory", JSON.stringify(newLocalData));
    }
  };

  const mapCurrentWeather = () => {
    const obj = dataResult.current || {};

    // console.log(dataResult);
    return (
      <>
        {" "}
        <li>
          {" "}
          <span>Clouds</span> <span>{obj["clouds"]}</span>
        </li>
        <li>
          {" "}
          <span>Dew point</span> <span>{obj["dew_point"]}</span>
        </li>
        <li>
          {" "}
          <span>Dt</span> <span>{obj["dt"]}</span>
        </li>
        <li>
          {" "}
          <span>feels nlike</span> <span>{obj["feels_like"]}</span>
        </li>
        <li>
          {" "}
          <span>feels like</span> <span>{obj["humidity"]}</span>
        </li>
        {/* <li>
          {" "}
          <span>Rain</span> <span>{obj["rain"]}</span>
        </li> */}
        <li>
          {" "}
          <span>sunrise</span> <span>{obj["sunrise"]}</span>
        </li>
        <li>
          {" "}
          <span>temp</span> <span>{obj["temp"]}</span>
        </li>
        <li>
          {" "}
          <span>humidity</span> <span>{obj["humidity"]}</span>
        </li>
        <li>
          {" "}
          <span>sunset</span> <span>{obj["sunset"]}</span>
        </li>
        <li>
          {" "}
          <span>uvi</span> <span>{obj["uvi"]}</span>
        </li>
        <li>
          {" "}
          <span>visibility</span> <span>{obj["visibility"]}</span>
        </li>
        <li>
          {" "}
          <span>wind deg</span> <span>{obj["wind_deg"]}</span>
        </li>
        <li>
          {" "}
          <span>wind gust</span> <span>{obj["wind_gust"]}</span>
        </li>
        <li>
          {" "}
          <span>wind speed</span> <span>{obj["wind_speed"]}</span>
        </li>
        {obj["weather"] &&
          obj["weather"].map((items, index) => (
            <li key={index}>
              <span>weather Description</span>{" "}
              <span>{items["description"]}</span>
            </li>
          ))}
        <li>
          {" "}
          <span>wind speed</span> <span>{obj["wind_speed"]}</span>
        </li>
      </>
    );
  };

  React.useEffect(async () => {
    places_id &&
      (await Getweatherinfo(places_id)
        .then((response) => {
          //   console.log(response.data);
          setDataResult(response.data);
        })
        .then(() => {
          CreateViewHistory(places_id, description);
        })
        .catch((error) => console.log(error)));
  }, []);
  return (
    <div>
      <Navbar />
      {/* <p>{places_id}</p> */}
      <div className="detailpage my-80">
        <div className="container">
          <div className=" back-primary detail-main">
            <div className="col-lg-10 mb-5">
              {" "}
              <h3>Weather info for {description}</h3>
              <div className="line3" />
              <div style={{ float: "right" }} className="col-4 ">
                <Link
                  to={`/HistoricGraph/${places_id}?description=${description}`}
                >
                  <button className="btn btn-secondary">
                    view 5 day historic weather
                  </button>
                </Link>
              </div>
            </div>
            <div className="col-lg-7 mb-1">
              <h4 className="mb-2">Current: </h4>
              <ul className="result-list">{mapCurrentWeather()}</ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Detailview;
