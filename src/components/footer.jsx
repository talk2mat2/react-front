import React from "react";

const Footer = () => {
  return (
    <footer className="footer text-center mt-180">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <p>Weatherman @ 2021. All Rights Reserved.</p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
