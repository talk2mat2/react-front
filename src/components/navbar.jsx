import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="nav-bar Row-direction justify-content-between align-items-center">
      <div className="brand-logo">
        <Link to="/">
          <img src="/weman.png" alt="img" />
        </Link>
      </div>

      <ul className="nav-list Row-direction ">
        <li className="nav-items">
          <Link to="/">home</Link>
        </li>
        <li className="nav-items">
          <Link to="/ViewHistory">History</Link>
        </li>
        <li className="nav-items">
          <a href="/#howitworks">How it works</a>
        </li>
        <li className="nav-items">
          <a href="/#about-us">About us</a>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
