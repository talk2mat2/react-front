import axios from "axios"


export const GoogleAutoComplete = async (query) => {
   const response= await axios.get(`/api/GoogleAutoComplete?search=${query}`)
        .then(response =>  response.data
    )
        .catch(error => {throw error})
    return response
}
export const Getweatherinfo = async (places_id) => {
   const response= await axios.get(`/api/Getweatherinfo?places_id=${places_id}`)
        .then(response =>  response.data
    )
        .catch(error => {throw error})
    return response
}
export const Historicweatherinfo = async (places_id) => {
   const response= await axios.get(`/api/Historicweatherinfo?places_id=${places_id}`)
        .then(response =>  response.data
    )
        .catch(error => {throw error})
    return response
}
